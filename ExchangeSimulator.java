import java.time.LocalTime;

public class ExchangeSimulator {
    public static final Object Lock = new Object();
    ActionAll actionAll = new ActionAll();
    public static void main(String[] args) {
        Action<ActionAll> AllAction = new Action<>();
        AllAction.додати(new AAPL(100,141));
        AllAction.додати(new IBM(200,137));
        AllAction.додати(new COKE(1000,387));
        new Thread(()->{
            synchronized (Lock){
                try {
                    Lock.wait(30000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                changePrice();
            }
        },"Потік-1").start();
    }

    private static void changePrice () {
        changePrice();
    }
}
