public class BuyersAll {
private String name;
private int amount;
private int price;
public BuyersAll(String name, int amount, int price){
    this.amount=amount;
    this.price=price;
    this.name=name;
}

    public BuyersAll() {

    }

    public String getName(){
    return name;
    }

    public int getAmount() {
        return amount;
    }

    public int getPrice() {
        return price;
    }
}
